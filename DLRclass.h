#ifndef DLRCLASS_H_INCLUDED
#define DLRCLASS_H_INCLUDED

#include <iostream>
#include <cstring>
#include<stdio.h>

using namespace std;

class emptylistexception{
public:
char error_message[50];
emptylistexception(){*error_message = 0;}
emptylistexception(char *s){strcpy(error_message,s);}
};

template <typename Key,typename Info>
class DLR{

private:
    struct Node{
Key key;
Info info;
Node* next;
Node* back;
};
    Node * last;
    int count;
    Node * first;

public:

  //ITERATOR BEGIN
    template<typename K, typename I>

    class iterator
    {
        friend class DLR;

        Node *current;

public:
        iterator () {current = nullptr;}
        //Default constructor for class iterator
         iterator(Node *c) {current = c;}
         //Parametrized constructor  for class Iterator
        // operators
         const iterator& operator=(const iterator& someIterator){
                    current = someIterator.current;
                    return *this;
                }
        //overloaded assignment for class Iterator
        //another operators for class iterator
        iterator &operator ++ () // pre inc
        {
            next();
            return *this;
        }
        iterator operator ++ (int) // post inc
        {
            iterator before = *this;
            next();
            return before;
        }
        iterator &operator -- () // pre dec
        {
            back();
            return *this;
        }
        iterator operator -- (int) // post dec
        {
            iterator before = *this;
            back();
            return before;
        }

        bool operator == (const iterator &a) { return current == a.current; }

        bool operator != (const iterator &a) { return current != a.current; }

        void next() // moving forward
        {
            current = current->next;
        }
        void back() // moving backward
        {
            current = current->back;
        }

        void next(int n) // fwd n times
        {
            for (int i = 0; i < n; i++)
                next();
        }
        void back(int n) // bckwd n times
        {
            for (int i = 0; i < n; i++)
               back();
        }

        // getters
        K &key()
        {
            if (current==nullptr) throw emptylistexception("EXCEPTION!:Trying to access nullptr");
            return current->key;
        }
        I &info()
        {
            if (current==nullptr) throw emptylistexception("EXCEPTION!:Trying to access nullptr");
            return current->info;
        }

    };

//ITERATOR FINISH

    //for more comfortable use
    typedef iterator<Key, Info> Iterator;
    typedef iterator<const Key, const Info> Const_Iterator;

    //Methods
    Iterator begin() {Iterator it(first); return it;}
    //returns Iterator pointing on the first Node
    Const_Iterator const_begin() const  {Const_Iterator it(first); return it;}
   //returns constant Iterator(read-only) pointing on the first Node
    Iterator end() {Iterator it(last); return it;}
    //returns Iterator pointing on the last Node
    Const_Iterator const_end() const {Const_Iterator it(last); return it;}
   //returns constant Iterator(read-only) pointing on the last Node

    //Function Returns iterator to the element with specific key in the list
    Iterator findbykey(const Key& someKey) {
    Iterator it = begin();

     if(it == nullptr )
     {cout<<"Trying to perform action on Empty list"<<endl;
        return nullptr;}
    else
    if(this->searchByKey(someKey)==false)
        {   cout<<"NO SUCH A KEY IN THE LIST"<<endl;
            return nullptr;}
       else{
     do{

        if(it.key()==someKey)
            return it;

        it++;
     }while(it!=begin());} }


   //function to insert element after specific Key
  //returns iterator on the newly created node
  //function assumes that the list contains at least one element
 Iterator insert_after(const Key& keyAfter,const Key& someKey,const Info& someInfo)
 {

     Iterator Ait = findbykey(keyAfter);

     if(Ait == nullptr)
         return nullptr;
    else{
     this->PushAfter(keyAfter,someKey,someInfo);
     ++Ait;
     return Ait;}
}

   //function to insert element before specific Key
  //returns iterator on the newly created node
  //function assumes that the list contains at least one element
 Iterator insert_before(const Key& keyBefore,const Key& someKey,const Info& someInfo)
 {

     Iterator Bit = findbykey(keyBefore);

     if(Bit == nullptr)
         return nullptr;
    else{
     this->PushBefore(keyBefore,someKey,someInfo);
     return --Bit;}
 }

//deletes a node with given key and returns iterator to the one next to it
Iterator RemoveNodeByKey(const Key& someKey)
{
     Iterator Dit = findbykey(someKey);

     if(Dit == nullptr)
         return nullptr;
    else{
     this->deleteNodeByKey(someKey);
     Dit++;
     return Dit;}
}

//this function deletes an element in th list and replace it with a new one
Iterator Replace(Iterator it,const Key& replaceKey,const Info& replaceInfo)
{
    this->PushAfter(it,replaceKey,replaceInfo);
    this->RemoveNodeByKey(it.key());

}

   //changes the data in the element on which Iterators self is pointing to
   //returns Iterator to this element
   Iterator change(const Key& wheretochange, const Key& someKey,const Info& someInfo)
    {
        Iterator it = findbykey(wheretochange);
        if(it == nullptr)
         return nullptr;
        else{
        it.current->key = someKey;
        it.current->info = someInfo;}
    }
     //overladed function PushAfter now takes iterator and insert new element in the position after it
     //return iterator on newly created node
     Iterator PushAfter(Iterator It,const Key& someKey,const Info& someInfo)
     {
         this->insert_after(It.key(),someKey,someInfo);
     }
     //overladed function PushBEfore now takes iterator and insert new element in the position before it
     //return iterator on newly created node
     Iterator PushBefore(Iterator It,const Key& someKey,const Info& someInfo)
     {
         this->insert_before(It.key(),someKey,someInfo);
     }

    DLR();//default constructor
    ~DLR(); //destructor

     DLR(const DLR<Key,Info>& otherList);
      //copy constructor

   //~DLR();//destructor
    void initializeList();
      //Initialize the list to an empty state.
      //such that: first = NULL, last = NULL, count = 0;

  void copyList(const DLR<Key,Info>& otherList);
      //Function to make a copy of otherList.
      //Such that: A copy of otherList is created and assigned
      //    to this list.

  const DLR<Key,Info>& operator=(const DLR<Key,Info>&);
      //Overload the assignment operator.

    bool isEmpty() const;
     //Function to determine whether the list is empty.
      // Returns true if the list is empty, otherwise
      //    it returns false.
    int length() const;
     //Function to return the number of nodes in the list.
      // The value of count is returned.
    Info FirstElement() const;
      //Function to return info of the first element of the list.
      //If the list is empty, the function returns 0;
      //    otherwise, the first element of the list is returned (and displayed by the function).

    Info LastElement() const;
      //Function to return info of the last element of the list.
      // The list must exist and must not be empty.
      //If the list is empty, the program
      //               terminates; otherwise, the last
      //               element of the list is returned.

    Info ReturnInfoByKey(const Key& someKey);
    // Function which returns info of the key which was passed to this function
    // if in the list there are more than 1 elements with the key that was passed
    // function displays their infos in order they appear in the list
    // and returns the info of the element that appeared first
    // if there is only one element with the given key,function displays its info
    // and returns it;
    //if the list is empty function returns 0

    bool searchByKey(const Key& searchItemKey) const;
        //Function to determine whether searchItem is in the list.
      //Returns true if searchItem is in the list,
      //    otherwise the value false is returned.

	int countKey(const Key& givenKey);
    // counts all elements with given Key
    //obviously if there is no element with given key,returns 0

    void PushFirst(const Key& FsomeKey,const Info& FsomeInfo);
      //Function to insert newItem at the beginning of the list.
      //  newItem is inserted at the beginning of the list, last points to the
      //    last node in the list, and count is incremented by 1.

   void PushAfter(const Key& KeyAfter, const Key& AsomeKey, const Info& AsomeInfo);
      //Function to insert newNode at the specific position(after the required key)

   void PushBefore(const Key& KeyBefore, const Key& BsomeKey, const Info& BsomeInfo);
      //Function to insert newNode at the specific position(after the required key)

    void PushLast(const Key& LsomeKey,const Info& LsomeInfo);
    //Function to insert newItem at the end of the list.
      //  newItem is inserted at the end of the list, last points to the
      //    last node in the list, and count is incremented by 1.

   void deleteNodeByKey(const Key& DsomeKey);
        //Function to delete deleteItem from the list.
      //Postcondition: If found, the node containing deleteItem is
      //    deleted from the list; first points to the first node
      //    of the new list, and count is decremented by 1. If
      //    deleteItem is not in the list, an appropriate message
      //    is printed.

      void delete_first();
      // delete first element in the list

      void delete_last();
      	// removes last element in the list

      void destroyList();
      //Function to delete all the nodes from the list.
      //so: first = NULL, last = NULL, count = 0;

      void print() const;
      //function to output all nodes which the list contains
      void ReversePrint() const;
      //Function to print the list in reverse direction

      void printByKey(const Key& PsomeKey) const;
      //function to output one particular node

      void delete_all_with_key(Key givenKey);
      	// removes all elements with Key == givenKey
      void removeallwithkey(Key givenKey);

      void insert(Key place,Key before,Key after);

      	//Class DLR METHODS END
};


#endif // DLRCLASS_H_INCLUDED
