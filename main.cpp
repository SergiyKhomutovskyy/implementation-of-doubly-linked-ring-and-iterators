#include "DLRclass.h"
#include "DLRmethods.h"

using namespace std;


int main()
{

   DLR<int,int> ring;
    ring.PushFirst(100,10);
    ring.PushLast(200,20);
    ring.PushLast(300,30);
    ring.PushLast(400,40);
    ring.PushLast(500,50);
    ring.PushLast(600,60);
    ring.PushLast(700,70);
    ring.PushLast(800,80);
    ring.PushLast(900,90);
    ring.PushFirst(100,9);
    ring.PushAfter(800,100,8);
    ring.PushLast(100,7);
    ring.PushAfter(400,400,4);
    ring.PushAfter(400,400,5);
    ring.PushAfter(400,100,5);
    ring.PushAfter(400,100,4);
    ring.PushAfter(400,100,3);

   ring.removeallwithkey(100);
   ring.print();
   cout<<ring.length()<<endl;

    return 0;
}
