#ifndef DLRMETHODS_H_INCLUDED
#define DLRMETHODS_H_INCLUDED

template <typename Key,typename Info>
DLR<Key,Info>::DLR() //default constructor
{
    first = nullptr;
    last = nullptr;
    count = 0;
    cout<<"...Default Constructor did his job...."<<endl;
}

template <typename Key,typename Info>
DLR<Key,Info>::DLR(const DLR<Key,Info>& otherList)
{
     cout<<"Copy Constructor did his job"<<endl;
    first = nullptr;
    copyList(otherList); //copy constructor


}

template <typename Key,typename Info>
const DLR<Key,Info>& DLR<Key,Info>::operator=(const DLR<Key,Info>& otherList)
{
    if (this != &otherList) //avoid self-copy
    {
        copyList(otherList);
    }//end else
    cout<<"Overloaded Assignment operator did his job"<<endl;

     return *this;
}


template <typename Key,typename Info>
void DLR<Key,Info>::initializeList()
{
	destroyList(); //if the list has any nodes, delete them
}


template <typename Key,typename Info>
void DLR<Key,Info>::destroyList()
{
    Node* temp; //pointer to deallocate the memory
                            //occupied by the node
    if(first==nullptr)
    return;
    else
    {last->next = nullptr; //now it is not a ring
       //while there are nodes in the list
   while(first!=nullptr)
    {
        temp = first;        //set temp to the current node
        first = first->next; //advance first to the next node
        delete temp;   //deallocate the memory occupied by temp
    }
   //cout<<first<<endl;

     //initialize last to NULL; first has already been set to NULL by the while loop
    last = nullptr;
    count = 0;}
}

template <typename Key,typename Info>
DLR<Key,Info>::~DLR() //destructor
{
   destroyList();
     cout<<"...Destructor did his job...."<<endl;
}  //end destructor


template <typename Key,typename Info>
void DLR<Key,Info>::copyList(const DLR<Key,Info>& otherList)
{
    Node *newNode; //pointer to create a node
    Node *current; //pointer to traverse the list
    Node *beforeCurrent;

    if (first != nullptr) //if the list is nonempty, make it empty
       destroyList();

    if (otherList.first == nullptr) //otherList is empty
    {
        first = nullptr;
        last = nullptr;
        count = 0;
    }
    else
    {
        current = otherList.first; //current points to the
                                   //list to be copied
        count = otherList.count;

             //copy the first node
        first = new Node;  //create the node

        first->key  =  current->key; //copy the info
        first->info = current->info;
        first->next = first;
        first->back = first;

        last = first;

        beforeCurrent = first;
        current = current->next; //make current point to the next node

        //copy the remaining list
        while (current != otherList.first) {



            newNode = new Node;  //create a node
            newNode->key = current->key; //copy the info
            newNode->info = current->info;
            newNode->next = nullptr;
            newNode->back = nullptr;
                       //set the link of
                                        //newNode to NULL
            last->next = newNode;
            newNode->back = beforeCurrent;
            last = newNode;
            last->next = first;
            first->back = newNode;


            beforeCurrent = last;
            current = current->next;   //make current point
                                       //to the next node
        } //end while
    }//end else
}//end copyList


template <typename Key,typename Info>
bool DLR<Key,Info>::searchByKey(const Key& searchItemKey) const
{
    bool found = false;
    Node *current; //pointer to traverse the list

if(first == nullptr)
   {cout<<"THE LIST IS EMPTY FUNCTION WILL RETURN FALSE"<<endl;
   return false;}


    current = first; //set current to point to the first node in the list to start from the first node

       //search the list
        do
        {if (current->key == searchItemKey) //searchItem is found
            found = true;
        else
            current = current->next; //make current point to
            }while (current != first && !found);                         //the next node

      if (found)

      found = (current->key == searchItemKey); //test for equality

    return found;}
//end search


template <typename Key,typename Info>
bool DLR<Key,Info>::isEmpty() const
{
    return (first == nullptr);
}

template <typename Key,typename Info>
int DLR<Key,Info>:: length() const
{
    return count;
}  //end length

//PushFirst Without Iterator
template <typename Key,typename Info>
void DLR<Key,Info>::PushFirst(const Key& FsomeKey,const Info& FsomeInfo)
{
    //Node * current;
    Node * newNode;

    newNode = new Node;
    newNode->key = FsomeKey;
    newNode->info = FsomeInfo;
    newNode->back = nullptr;
    newNode->next = nullptr;

    //current = first;

    if(first == nullptr) //empty list situation
      {first = newNode;
        last = newNode;
        newNode->next = newNode;
        newNode->back = newNode;
        count++;}
    else //pushing new element BEFORE first element

    {   newNode->next = first;
        first->back = newNode;
        last->next = newNode;
        newNode->back = last;
        first = newNode;
        count++;}
}

template <typename Key,typename Info>
void DLR<Key,Info>:: PushLast(const Key& LsomeKey,const Info& LsomeInfo)
{
   //Node* current;
   Node* newNode;

   newNode = new Node;
   newNode->key = LsomeKey;
   newNode->info = LsomeInfo;
   newNode->next = nullptr;
   newNode->back = nullptr;

   if(first == nullptr) //empty list
   {
       first = newNode;
       last = newNode;
       newNode->next = newNode;
       newNode->back = newNode;
       count++;

    }
    else //adding after last
    {

        last->next = newNode;
        newNode->back = last;
        first->back = newNode;
        newNode->next = first;
        last = newNode;
        count++;

    }

}

template <typename Key,typename Info>
void DLR<Key,Info>::PushAfter(const Key& KeyAfter,const Key& AsomeKey, const Info& AsomeInfo)
{
   Node* current;
   Node* newNode;

      newNode = new Node;
      newNode->key = AsomeKey;
      newNode->info = AsomeInfo;
      newNode->next = nullptr;
      newNode->back = nullptr;

   if(first == nullptr)
     {
         cout<<"USE PUSHFIRST OR PUSHLAST"<<endl;
         return;
     }
    if(!(searchByKey(KeyAfter)))
    {cout<<"NOT FOUND"<<endl;
        return;}
    else{
        if(first->key==KeyAfter)
        {
            first->next->back = newNode;
            newNode->next = first->next;
            first->next = newNode;
            newNode->back = first;
            count++;}
         else
         {
             current = first;
             do{
                    current = current->next;
                if(current->key==KeyAfter)
                    break;
               }while(current!=first);

            if(current->next == first)
                 {current->next = newNode;
                 newNode->back = current;
                 first->back = newNode;
                 newNode->next = first;
                 last = newNode;
                 count++;}
            else
                {current->next->back = newNode;
                newNode->next = current->next;
                current->next = newNode;
                newNode->back = current;
                count++;}

}
}
}

template <typename Key,typename Info>
void DLR<Key,Info>:: PushBefore(const Key& KeyBefore, const Key& BsomeKey, const Info& BsomeInfo){

   Node* current;
   Node* newNode;

      newNode = new Node;
      newNode->key = BsomeKey;
      newNode->info = BsomeInfo;
      newNode->next = nullptr;
      newNode->back = nullptr;

   if(first == nullptr)
     {
         cout<<"USE PUSHFIRST OR PUSHLAST"<<endl;
         return;
     }
    if(!(searchByKey(KeyBefore)))
    {cout<<"NOT FOUND"<<endl;
        return;}
    else{
        if(first->key==KeyBefore)
          {newNode->next = first;
           first->back = newNode;
           newNode->back = last;
           last->next = newNode;
           first = newNode;
           count++;}
         else
         {
             current = first;
             do{
                current = current->next;
                if(current->key==KeyBefore)
                    break;
               }while(current!=first);

                 current->back->next = newNode;
                 newNode->back = current->back;
                 current->back = newNode;
                 newNode->next = current;
                 count++;

}
}
}

template <typename Key,typename Info>
Info DLR<Key,Info>::FirstElement() const
{
    if(first == nullptr)
     {cout<<"CANNOT RETURN IINFO OF FIRST ELEMENT FROM EMPTY LIST (0 WILL BE RETURNED BY FUNCTION)"<<endl;
     return 0;}
     else
     {   cout<<"info of first element: "<<first->info<<endl;
        return first->info;} //return the info of the first node
}

template <typename Key,typename Info>
Info DLR<Key,Info>::LastElement() const
{
    if(first == nullptr)
     {cout<<"CANNOT RETURN IINFO OF LAST ELEMEN FROM EMPTY LIST (0 WILL BE RETURNED BY FUNCTION)"<<endl;
     return 0;}
     else
        {cout<<"info of last element: "<<last->info<<endl;
         return last->info;} //return the info of the last node
}

template <typename Key,typename Info>
Info DLR<Key,Info>:: ReturnInfoByKey(const Key& someKey)
{
    if(first == nullptr)
        {cout<<"CANNOT RETURN IINFO OF PARTICULAR ELEMENT FROM EMPTY LIST (0 WILL BE RETURNED BY FUNCTION)"<<endl;
        return 0;}

if(searchByKey(someKey))
{
    cout<<"In the list there are: "<< this->countKey(someKey) <<" element(s) of Given Key"<<endl;
    Node* current;

    if((this->countKey(someKey))>1)
    {
        int i=1;
        current = first;
        do{
            if(current->key==someKey){
                cout<<"Info of the "<<i<<" element with the given key: "<<current->info<<endl;
                return current->info;
                i++;}
            current=current->next;

        }while(current!=first);
    }
    else{
    bool found = false;
    Node* current;
    current = first; //set current to point to the first node in the list to start from the first node

       //search the list
        do
        {if (current->key == someKey) //searchItem is found
            found = true;
        else
            current = current->next; //make current point to
            }while (current != first && !found);                         //the next node
    cout<<"And its info is(will be returned by the function): "<<current->info<<endl;
    return current->info;}}
    else
    {
        cout<<"IN THE LIST THERE IS(ARE) NOT ELEMENTS WITH SUCH A KEY:0 WILL BE RETURNED"<<endl;
        return 0;
    }
}

template <typename Key,typename Info>
int DLR<Key,Info>:: countKey(const Key& givenKey) // counts all elements with given Key
	{
		if (first==nullptr)
        {cout<<"COUNT KEY:LIST IS EMPTY(FUNCTION WILL RETURN 0}"<<endl;
        return 0;}
		int counter = 0;
		Node* Current = first;


		do{
			if (Current->key == givenKey)
                counter++;
			Current = Current->next;
		}while (Current!=first);

		return counter;
	}



template <typename Key,typename Info>
void DLR<Key,Info>::deleteNodeByKey(const Key& someKey)
{
    Node* current = nullptr; //pointer to traverse the list
    Node* beforeCurrent = nullptr; //pointer just before current
    bool found;

    if(first == nullptr)
    cout<<"deleteNodeByKey:CANNOT DELETE FROM EMPTY LIST"<<endl;

     else {
     if(first->next == first) //if there is only one element in the list
        {
          current = first;
          first->next = nullptr;
          first = first->next;
          last = nullptr;
          delete current;
                count--;}
           else  //if the first element is not the last one
          {
             //last->next = nullptr;
             current = first;
              found = false;

              do{
                  if(current->key==someKey)
                    found = true;
                    else
                    {
                        beforeCurrent=current;
                        current=current->next;
                    }
              }while(current!=first && found!=true);

             if(found==true)
            { if(last->next==current) //so we are about to delete the first element
               { first = current->next;
                  current->next->back = last;
                  last->next = first;
                   delete current;
                   count--;}
                else
                {
                    beforeCurrent->next = current->next;
                    if(current->next==first)
                        {last = beforeCurrent;
                          current->next->back = beforeCurrent;
                        delete current;
                        count--;}
                    else{
                    current->next->back = beforeCurrent;
                    delete current;
                    count--;}
                }}
                else
                cout<<"There is no element with such a key in the list"<<endl; }

}
}


template <typename Key,typename Info>
void DLR<Key,Info>:: delete_first() // removes first element in the list
	{
		if (first == nullptr)
        {
            cout<<"Cannot delete first element from an empty list"<<endl;
            return;
        }

		Node* Current = nullptr;
		if(first->next==first)
        {
            Current=first;
            first = nullptr;
            last = nullptr;
            count = 0;
            delete Current;

        }
        else
        {Current = first;
		first = Current->next;
		last->next = first;
		delete Current;
		if(first->next == first)
        {last = first;
        last->next = first;}
		count--;}
	}

template <typename Key,typename Info>
void DLR<Key,Info>::delete_last() // removes last element in the list
{
	 if (first == nullptr)
        {
            cout<<"Cannot delete last element from an empty list"<<endl;
            return;
        }
    Node* Current = nullptr;
    if(first->next==first) //if there is one element in the list
        {
        Current = first;
        first = nullptr;
        last = nullptr;
        count = 0;
        delete Current;
        }
      else
{
        last->next = nullptr;
        Current = first;
		Node *beforeCurrent = nullptr;

		while (Current->next!=nullptr){
			beforeCurrent = Current;
			Current = Current->next;
		}
		beforeCurrent->next = first;
		first->back = beforeCurrent;
		delete Current;
		last = beforeCurrent;
		count--;
}
}

template <typename Key,typename Info>
void DLR<Key,Info>::delete_all_with_key(Key givenKey) // removes all elements with Key == givenKey
{
		if(first == nullptr)
        {cout<<"delete_all_with_key: Cannot delete from an empty list"<<endl;
            return;}
        else{
        bool Exist = searchByKey(givenKey);

		while (Exist)
		{
			deleteNodeByKey(givenKey);
			Exist = searchByKey(givenKey);
		}
	}
}



template <typename Key,typename Info>
void DLR<Key,Info>::print() const
{


    Node *current; //pointer to traverse the list

    current = first;    //set current so that it points to
                        //the first node
    if(current == nullptr)
    {
        cout<<"The list is empty"<<endl;
    }
    else
    {
        int i = 1;
     //while more data to print


  do
    {
        cout<< "ELEMENT N: "<<i<<endl<<"KEY: "<<current->key<<endl<<"INFO: "<<current->info<<endl;
        current = current->next;
        i++;
    } while (current != first);
      }
}  //end print

template <typename Key,typename Info>
void DLR<Key,Info>::ReversePrint() const
{

    if(first==nullptr)
    {
        cout<<"The List is Empty"<<endl;
    }
    else
{
    Node * current;
    current = last;
    int i = 1;
    do
    {

        cout<< "ELEMENT N: "<<i<<endl<<"KEY: "<<current->key<<endl<<"INFO: "<<current->info<<endl;
        current = current->back;
        i++;

    }while(current!=last);
}
}

template <typename Key,typename Info>
void DLR<Key,Info>::printByKey(const Key& PsomeKey) const{

if(first==nullptr)
  {cout<<"Cannot print by key from an empty list"<<endl;
    return;}
else{
Node* current;
current = first;


  do{
    if(current->key == PsomeKey)
    {
        cout<<"SEARCHED ELEMENT: "<<endl;
        cout<<"KEY: "<<current->key<<endl<<"INFO: "<<current->info<<endl;
        break; }

        else
            current=current->next;
       if(current == first)
          cout<<"NOT FOUND"<<endl;

   }while(current != first );}

  }

template<typename Key,typename Info>
void DLR<Key,Info>::insert(Key place,Key before,Key after)
{
          Node* newNode1 = new Node;
          newNode1->key = before;
          newNode1->next = nullptr;
          newNode1->back = nullptr;

          Node* newNode2 = new Node;
          newNode2->key = after;
          newNode2->next = nullptr;
          newNode2->back = nullptr;
          Node *current;
          bool found = false;

    if(first == nullptr)
    {
        cout<<"the lust is empty"<<endl;
        delete newNode1;
        delete newNode2;
        return;
    }
    else
    {
        current = first;
        if(current->key==place)
        {
            newNode2->next = first->next;
            newNode2->back = first;
            if(newNode2->next == first)
            {first->back = newNode2;
             first->next = newNode2;
             last = newNode2;}
            first->next->back = newNode2;
            first->next = newNode2;
            count++;


            newNode1->next = first;
            newNode1->back = last;
            last->next = newNode1;
            first->back = newNode1;
            first = newNode1;
            count++;
            }
        else
{
        current = current->next;
        while(current!=first && found == false) {

                if(current->key == place) {
                    found = true;
                    break;
                } else {
                    current = current->next;
                }
            }
            if(found)
               {cout<<"FOUND"<<endl;
               newNode2->next = current->next;
               newNode2->back = current;
               current->next->back = newNode2;
               current->next = newNode2;
               if(newNode2->next ==first)
               {last = newNode2;}
               count++;

               newNode1->back = current->back;
               if (newNode1->back == first)
               {first = newNode1;}
               current->back->next = newNode1;
               newNode1->next = current;
               current->back = newNode1;
               count++;}
   if(current == first)
    cout<<"No such a key in the list"<<endl;
}
}
}


template <typename Key,typename Info>
void DLR<Key,Info>::removeallwithkey(Key givenKey) // removes all elements with Key == givenKey
{
		Node*current;
		Node*beforecurrent;
		Node*temp;

		if(first == nullptr)
        {cout<<"removeallwithkey: Cannot delete from an empty list"<<endl;
            return;}
        if(searchByKey(givenKey)==false)
        {
            cout<<"removeallwithkey:there is no such a key"<<endl;
            return;
        }
        else{
                current = first;

                //beforecurrent = first;
             do{
                if(current->key==givenKey)
                {
                   temp = current;
                   if(current->next == first)
                    last = current->back;
                   if(current->back == last)
                      first = current->next;
                   current->back->next = current->next;
                   current->next->back = current->back;
                   count--;
                   delete temp;
                    }
                current = current->next;

                  }while((searchByKey(givenKey))==true);

}
}

#endif // DLRMETHODS_H_INCLUDED
